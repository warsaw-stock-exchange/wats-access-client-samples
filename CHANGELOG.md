# Changes history for WATS Access Clients samples

## 1.5

* **Change:** Access Clients (connectivity example code) repositories updated and adapted to version 1.5
* **Change:** QuickFIX XML templates removed because they are published on official project's website (https://gpwwats.pl/dokumentacja-i-faq)

## 1.3

* **Change:** Access Clients (connectivity example code) repositories updated and adapted to version 1.3
* **Change:** QuickFIX XML templates updated to 1.3

## 1.2

* **Change:** Access Clients (connectivity example code) repositories updated and adapted to version 1.2
* **Change:** QuickFIX XML templates updated to 1.2

## 1.1

* **Change:** Access Clients (connectivity example code) repositories updated and adapted to version 1.1
* **Change:** Documentation updated to version 1.1
* **Change:** QuickFIX XML templates updated to 1.1

## 1.0

* **Change:** Access Clients (connectivity example code) repositories updated and adapted to version 1.0
* **Change:** Documentation updated to version 1.0
* **Change:** QuickFIX XML templates updated to 1.0

## 0.62

* **New:** Changelog documents added
* **Change:** Access Clients (connectivity example code) repositiories updated and adapted to version 0.62
* **Change:** Documentation updated to version 0.62
* **Change:** QuickFIX XML templates updated to 0.62

## 0.59

* **New:** Licensing information added
* **Change:** Access Clients (connectivity example code) repositiories updated and adapted to version 0.59
* **Change:** Documentation updated to version 0.59
* **Change:** QuickFIX XML templates updated to 0.59

## 0.57

* **New:** New documentation component "GPW WATS Advancement"
* **Change:** Access Clients (connectivity example code) repositories updated and adapted to version 0.57
* **Change:** Documentation updated to version 0.57
* **Change:** QuickFIX XML templates updated to 0.57

## 0.56

* **Change:** Access Clients (connectivity example code) repositiories updated and adapted to version 0.56
* **Change:** Documentation updated to version 0.56
* **Change:** QuickFIX XML templates updated to 0.56

## 0.53

* **Change:** Documentation updated to version 0.53

## 0.41

* **Change:** Multicast distribution layer based on n2n protocol added
* **Change:** Documentation updated to version 0.41

## 0.36

* Initial release
