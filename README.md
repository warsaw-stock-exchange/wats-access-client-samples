# GPW WATS Access Clients samples
Source code and documentation for accessing GPW WATS.

## About
Repository combines documentation and source code examples for accessing Warsaw Automated Trading System (GPW WATS) for Independent Software Vendors (ISV).
