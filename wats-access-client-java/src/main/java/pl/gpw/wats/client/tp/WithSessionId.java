package pl.gpw.wats.client.tp;

public interface WithSessionId {
    Integer getSessionId();
    void setSessionId(Integer sessionId);

}
