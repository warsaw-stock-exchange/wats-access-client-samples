# Changes history for WATS Access Client Rust sample code

## 1.3.0

* **Change:** Message definitions (contract) updated to version 1.3.10

## 1.2.0

* **Change:** Message definitions (contract) updated to version 1.2.0

## 1.1.0

* **Change:** Message definitions (contract) updated to version 1.1.5

## 1.0.0

* **Change:** Message definitions (contract) updated to version 1.0.0

## 0.62

* **Change:** Message definitions (contract) updated to version 0.62

## 0.61

* **Change:** Message definitions (contract) updated to version 0.61

## 0.36

* Initial release