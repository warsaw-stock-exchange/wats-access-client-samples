# Changes history for WATS Access Client common data

## 1.5

**No changes**

## 1.3

**No changes**

## 1.2

**No changes**

## 1.1

* **Change:** Documentation and QuickFIX XML replaced with URL to official documentation distribution

## 1.0

* **Change:** Documentation updated to version 1.0
* **Change:** QuickFIX XML templates updated to 1.0

## 0.62

* **New:** Change log documents added
* **Change:** Documentation updated to version 0.62
* **Change:** QuickFIX XML templates updated to 0.62

## 0.59

* **New:** Licensing information added
* **Change:** WATS Core components updated to version 0.59
* **Change:** Access Clients (connectivity example code) repositories updated and adapted to version 0.59
* **Change:** Documentation updated to version 0.59
* **Change:** QuickFIX XML templates updated to 0.59

## 0.57

* **New:** New documentation component "GPW WATS Advancement"
* **Change:** WATS Core components updated to version 0.57
* **Change:** Access Clients (connectivity example code) repositories updated and adapted to version 0.57
* **Change:** Documentation updated to version 0.57
* **Change:** QuickFIX XML templates updated to 0.57

## 0.56

* **Change:** WATS Core components updated to version 0.56
* **Change:** Access Clients (connectivity example code) repositories updated and adapted to version 0.56
* **Change:** Documentation updated to version 0.56
* **Change:** QuickFIX XML templates updated to 0.56

## 0.53

* **Change:** WATS Core components updated to version 0.53
* **Change:** Documentation updated to version 0.53

## 0.41

* **Change:** WATS Core components updated to version 0.41
* **Change:** Multicast distribution layer based on n2n protocol added
* **Change:** Documentation updated to version 0.41

## 0.36

* Initial release
